import React from 'react';
import { Map, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import Markers from './Markers';

const MapView = (props) => {

  const currentLocation = {lat: Number(props.lat), lng: Number(props.long) }
  const zoom = 14;

  return (
    <Map center={currentLocation} zoom={zoom}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
      />
      <Markers currentLocation={[Number(props.lat), Number(props.long)]} />
    </Map>
  );
}

export default MapView;
