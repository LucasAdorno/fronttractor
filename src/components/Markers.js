import React from 'react'
import { Marker } from 'react-leaflet';
import LocationIcon from './LocationIcon';

const Markers = (props) => {

  const markers =
    <Marker position={props.currentLocation} 
    icon={LocationIcon} >
    </Marker>

  return <>{markers}</>
};

export default Markers;
