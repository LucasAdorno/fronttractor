import React, { useState, useEffect, useRef } from 'react';
import MapView from './components/MapView';
import Header from './components/Header';
import './App.css';
import api from './services/api';

function App() {

  let [infos, setInfos] = useState([{ lat: 0, long: 0 }]);
  const [attDate, setAttDate] = useState('');
  const [attHour, setAttHour] = useState('');
  const inputRef = useRef();

  const handleChangeInput = (e) => {
    e.preventDefault();
    api.get('users').then(res => {
      let attInfos = res.data;
      const searchId = inputRef.current.value
      if (searchId !== '') {
        let infosFiltered = attInfos.filter(i => i.id === searchId);
        setInfos(infosFiltered);
      }
      else {
        setInfos(attInfos);
      }
    })
  }

  const handleClickButton = () => {
    api.get('users').then(res => {
      let attInfos = res.data;
      inputRef.current.value = '';
      setInfos(attInfos);
    })
    const data = new Date();
    setAttDate(`${data.getDate()>=10 ? data.getDate() : '0'+data.getDate()}/${data.getMonth()>=10 ? data.getMonth() : '0'+data.getMonth()}/${data.getUTCFullYear()}`)
    setAttHour(`${data.getHours()}h${data.getMinutes()>=10 ? data.getMinutes() : '0'+data.getMinutes()}`)
  }

  useEffect(() => {
    api.get('users').then(res => {
      let attInfos = res.data;
      setInfos(attInfos);
    })
    let data = new Date();
    setAttDate(`${data.getDate()>=10 ? data.getDate() : '0'+data.getDate()}/${data.getMonth()>=10 ? data.getMonth() : '0'+data.getMonth()}/${data.getUTCFullYear()}`)
    setAttHour(`${data.getHours()}h${data.getMinutes()>=10 ? data.getMinutes() : '0'+data.getMinutes()}`)
  }, [])

  return (
    <>
      <Header />
      <div id="inputs-container">
        <input onChange={(e) => handleChangeInput(e)} ref={inputRef} type='number' placeholder='Trator Nº' />
        <div>
          <button onClick={() => handleClickButton()}>Atualizar lista</button>
          <h6>Ultima atualização feita dia {attDate} às {attHour} </h6>
        </div>
      </div>
      <div className="App">
        {infos.map(i =>
          <div className='info-container'>
            <h3 className='id-tractor'>Trator: {i.id}</h3>
            <MapView lat={i.lat} long={i.long} />
            <div className='date-hour-div'>
              <h5>{i.date}</h5>
              <h5>{i.hour}</h5>
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default App;
